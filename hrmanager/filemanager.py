import glob


class FileManager(object):

    def __init__(self, hrroot) -> None:
        super().__init__()
        self.hrroot = hrroot

    def get_rig_files(self):
        return sorted(glob.glob('{}/Rigs/*.rig'.format(self.hrroot)))

    def get_block_files(self):
        return sorted(glob.glob('{}/Blocks/*.rig'.format(self.hrroot)))

    def get_impulse_response_files(self):
        return sorted(glob.glob('{}/Impulse Responses/**/*.wav'.format(self.hrroot), recursive=True))

    def get_loop_files(self):
        return sorted(glob.glob('{}/Loops/*.wav'.format(self.hrroot)))

    def get_setlist_files(self):
        return sorted(glob.glob('{}/Setlists/*.setlist'.format(self.hrroot)))

    def get_lock_screen_logo_file(self):
        return '{}/LockScreenLogo.png'.format(self.hrroot)
