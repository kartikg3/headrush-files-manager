# Standard modules
import json
import re
import glob
import os


# TODO: check HR support and handling of nested directories
# TODO: check HR support and handling of [USER] and [IR ROOT] structures

def get_path_from_ir_string(ir_string, hrroot):
    print()   
    mo = re.match(r"\[directory\]\((.+)\)\[name\]\((.+)\)", ir_string)
    is_dir_ir_root = False
    globstr2 = ''
    if mo:
        if mo.group(1) in ['[USER]']:
            globstr = '{}/Impulse Responses/USER/**/{}.*'.format(hrroot, mo.group(2))
        elif mo.group(1) in ['[IR ROOT]']:
            is_dir_ir_root = True
            globstr = '{}/Impulse Responses/{}.*'.format(hrroot, mo.group(2))
            globstr2 = '{}/Impulse Responses/**/{}.*'.format(hrroot, mo.group(2))
        else:
            globstr = '{}/Impulse Responses/{}/**/{}.*'.format(hrroot, mo.group(1), mo.group(2))
    else:
        globstr = '{}/Impulse Responses/**/{}.*'.format(hrroot, ir_string)
    matches = glob.glob(globstr, recursive=True)
    if is_dir_ir_root and not matches:
        matches = glob.glob(globstr2, recursive=True)
    print('IR STR: ', ir_string)
    #print('GLOB STR: ', globstr)
    print('MATCHES: ', matches)
    for match in matches:
        if match.upper().endswith('.WAV'):
            print('MATCH: ', os.path.normpath(match))
            return os.path.normpath(match)
    return None


class ImpulseResponse(object):
    '''Object representing an impulse response file'''
    def __init__(self, ir_string, hrroot) -> None:
        super().__init__()
        self.ir_string = ir_string
        self.hrroot = hrroot
        self.ir_path = get_path_from_ir_string(ir_string=ir_string, hrroot=hrroot)
