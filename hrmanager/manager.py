# Package modules
from .filemanager import FileManager
from .rig import Rig, _get_rig_from_file


class Manager(object):

    def __init__(self, hrroot) -> None:
        super().__init__()
        self.hrroot = hrroot
        self.filemanager = FileManager(hrroot)
        self.rigs = self._get_rigs()

    def _get_rigs(self):
        return [_get_rig_from_file(rig, self.hrroot) for rig in self.filemanager.get_rig_files()]
