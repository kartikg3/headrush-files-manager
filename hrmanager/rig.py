# Standard modules
import json
import re

# Package modules
from .impulseresponse import ImpulseResponse


class Rig(object):
    '''Object representing a Headrush Rig'''
    def __init__(self, data, hrroot) -> None:
        super().__init__()
        self._data = data
        self.hrroot = hrroot
        self._content = json.loads(data['content'])
        patch_children = self._content["data"]["Patch"]["children"]
        self.name = patch_children["Rig"]["children"]["PresetName"]["string"]
        self.impulseresponses = []
        for key in patch_children.keys():
            if re.search(r'^IR$', key) or re.search(r"^IR [\d]+$", key):
                self.impulseresponses.append(ImpulseResponse(patch_children[key]['children']['IR']['string'], hrroot=self.hrroot))

    def __repr__(self) -> str:
        return r'<Rig: {}>'.format(self.name)

    def __str__(self) -> str:
        return str(self.name)


def _get_rig_from_file(rig_file, hrroot):
    with open(rig_file, 'r') as fp:
        data = json.load(fp)
    return Rig(data=data, hrroot=hrroot)
